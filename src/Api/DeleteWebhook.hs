{-# LANGUAGE NamedFieldPuns #-}

module Api.DeleteWebhook (deleteWebhook) where

import Servant (Handler)

import Server.Actions (ActionKind(DeleteWebhook), writeAction)
import Server.Context (Context(..))
import qualified Server.Internal as Server
import Server.Internal (Server(Server))
import Server.Response (Response(Ok))

import ServerState.User (User(User))
import qualified ServerState.User as User

deleteWebhook :: Context -> Handler (Response Bool)
deleteWebhook Context { botUser = User { User.userId }, server } = do
    writeAction userId actions DeleteWebhook
    return (Ok True)
    where Server { Server.actions } = server
